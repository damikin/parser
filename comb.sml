structure Parser =
struct
datatype Result =
	Success of string * string
	| Failure of string

datatype Parser = Parser of (string -> Result)

(* easy to run and get answers *)
fun run (Parser p) str = p str

(* concatenation *)
infix 6 <&>
fun p1 <&> p2 =
	let fun f str =
		let val r1 = run p1 str
		in case r1 of
			Failure err => Failure err
			| Success (v1,rest1) =>
				let val r2 = run p2 rest1
				in case r2 of
					Failure err => Failure err
					| Success (v2,rest2) => Success (v1 ^ v2,rest2)
				end
		end
	in Parser f
	end

(* alternation *)
infix 6 <|>
fun p1 <|> p2 =
	let fun f str =
		let val r1 = run p1 str
		in case r1 of
			Success r => r1
			| Failure _ => run p2 str
		end
	in Parser f
	end

(* kleene star *)
fun <*> p1 =
	let
		fun star str =
			let val r = run p1 str
			in case r of
				Failure _ => ("", str)
				| Success (v1, rest1) =>
					let val (v2, rest2) = star rest1
					in (v1 ^ v2, rest2)
					end
			end

		fun f str = Success (star str)
	in Parser f
	end

(* a parser for characters *)
fun pchar c =
	let fun f str =
		if String.size str = 0
		then
			if String.size c = 0
			then Success ("", "")
			else Failure "No more input"
		else
			let
				val c' = String.extract(str, 0, SOME 1)
			in
				if c = c'
				then  Success (c, String.extract(str, 1, NONE))
				else Failure ("Expecting '" ^ c ^ "', got '" ^ c' ^ "'")
			end
	in Parser f
	end

(* helpers *)
(* empty string *)
val emptyString = pchar ""

(* like [] *)
fun anyOf str =
	let
		fun choice [] = raise Empty
		| choice (p::ps) =
			let fun f (p', acc) = acc <|> p'
			in List.foldl f p ps
			end
		val cs = String.explode str
		val ss = List.map String.str cs
		val ps = List.map pchar ss
	in
		choice ps
	end

(* concatination for full strings *)
fun allOf str =
	let
		fun all [] = raise Empty
		| all (p::ps) =
			let fun f (p', acc) = acc <&> p'
			in List.foldl f p ps
			end
		val cs = String.explode str
		val ss = List.map String.str cs
		val ps = List.map pchar ss
	in
		all ps
	end

(* tests and examples *)
val pchrisall = allOf "Chris"
val pericall = allOf "Eric"
val pce = pchrisall <|> pericall
val pcestar = <*> pce
val pcestarj = pcestar <&> (allOf "James")

fun test1 () = run pcestar "Chris"
fun test2 () = run pcestar "Eric"
fun test3 () = run pcestar "ChrisEricJamesChris"
fun test4 () = run pcestarj "ChrisEricJamesChris"
fun test5 () = run ((allOf "Chris") <|> emptyString) "ChrisEric"
fun test6 () = run emptyString ""

end

